package visao;

import java.util.Scanner;

import modelo.Celsius;
import modelo.Fahrenheit;
import modelo.Kelvin;
import modelo.Termometro;

public class Main {

	
	public static void main(String[] args) {
	
		int opcao;
		float valor;
		Scanner ler = new Scanner(System.in);
		Termometro obj = null;
		
	
		System.out.println("Escolha uma opcao de entrada.");
		System.out.println("1-Celsius\n2-Kelvin\n3-Fahrenheit");
		opcao = Integer.parseInt(ler.nextLine());
		System.out.println("Insira o valor de entrada: ");
		valor = Float.parseFloat(ler.nextLine());
		
		switch(opcao){
			case 1:
				obj = new Celsius(valor);
			
				break;
			case 2:
				obj = new Kelvin(valor);
				
				break;
			case 3:
				obj = new Fahrenheit(valor);	
		}
		obj.calculoTemperatura();

	}

}
