package modelo;

public abstract class Termometro {

	private float temperatura;
	
	public Termometro(float temperatura){
		this.temperatura = temperatura;
	}

	public float getTemperatura() {
		return temperatura;
	}

	public void setTemperatura(float temperatura) {
		this.temperatura = temperatura;
	}
	
	 public void calculoTemperatura(){};
	
}
