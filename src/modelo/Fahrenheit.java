package modelo;

public class Fahrenheit extends Termometro {

		public Fahrenheit(float temperatura){
			super(temperatura);
		}
		
		public void calculoTemperatura(){
			float f = getTemperatura(); 	
			float k, c; 	
			c = 5*((f-32)/9);
			k = 273+(5*((f-32)/9)); 
			
			System.out.println("A temperatura em celsius e: "+c);
			System.out.println("A temperatura em kelvin e: "+k);
		}
}
