package modelo;

public class Kelvin extends Termometro {

		public Kelvin(float temperatura){
			super(temperatura);
		}
		
		public void calculoTemperatura(){
			float k = getTemperatura(); 	
			float c, f; 	
			c = k-273; 	
			f =32+((c/5)*9);
			
			System.out.println("A temperatura em celsius e: "+c);
			System.out.println("A temperatura em fahrenheit e: "+f);
		}
}