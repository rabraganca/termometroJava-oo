package modelo;

public class Celsius extends Termometro {

		public Celsius(float temperatura){
			super(temperatura);
		}
		
		public void calculoTemperatura(){
			float c = getTemperatura(); 
			float k, f; 	
			k = c+273; 	
			f = 32+((c/5)*9);
			
			System.out.println("A temperatura em Kelvin e: " +k);
			System.out.println("A temperatura em fahrenheit e: "+f);
		};
}
